import { combineReducers } from 'redux';
import store from '../StoreIndex';

const initialState = {
	count: 0
}

function incDecReducer(state = initialState, action) {
	let factor = (action && action.factor) ? action.factor : 0;
	switch (action.type) {
		case 'ADD': {
			return {
				count: state.count + factor
			}
		}

		case 'SUB': {
			return {
				count: state.count - factor
			}
		}

		default: return state
	}
}

function alertReducer(state = {value: 0}, action) {
	console.log(store)
	if(action.text){
		return {
			text: action.value
		}
	}

	return state
}

export default combineReducers({
	alertReducer,
	incDecReducer
});