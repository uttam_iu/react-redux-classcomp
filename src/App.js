import React, { Component } from 'react'
import './App.css';
import Counters from './components/Counters'
import Controller from './components/Controller'
import AlertMessage from './components/AlertMessage'
// import {createStore} from 'redux';
import { Provider } from 'react-redux';
import store from './store/StoreIndex';

class App extends Component {

	render() {

		return (
			<Provider store={store}>
				<div className="App">
					<Counters />
					<Controller />
					<AlertMessage />
				</div>
			</Provider>
		);
	}
}

export default App;

//work only redux in standalone
// class App extends Component {

// 	render(){

// 		//create reducer/minimum one ;
// 		const reducer = (state={}, action) =>{
// 			if(action.type === 'A'){
// 				return {
// 					...state,
// 					A: 'I am A' 
// 				}
// 			}
// 			else if (action.type === 'B'){
// 				return {
// 					...state,
// 					B: 'I am B'
// 				}
// 			}
// 			return state;
// 		}


// 		//create store with the reducer
// 		const store = createStore(reducer);

// 		//subscribe store
// 		store.subscribe(() => { console.log(store.getState()) })
// 		store.subscribe(() => { console.log(store.getState().A) })
// 		store.subscribe(() => { console.log(store.getState().B) })

// 		//dispatch an event/action / must type args
// 		store.dispatch({type: 'A'});
// 		store.dispatch({type: 'df'});
// 		store.dispatch({type: 'B'});
// 		store.dispatch({type: 'df'});

// 		return (
// 			<div className="App">
// 			  <Counters />
// 			  <Actions />
// 			</div>
// 		  );
// 	}
// }

// export default App;

