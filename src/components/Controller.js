import React, { Component } from 'react';
import { connect } from 'react-redux';


class Controller extends Component {
	render() {
		console.log(this.props)
		return (
			<div>
				<button onClick={() => this.props.add()}>+ INC</button>
				<button onClick={() => this.props.sub()}>- DEC</button>
				<button onClick={() => this.props.alert()}>- ALERT</button>
			</div>
		);
	}
}


function mapDispatch(dispatch) {
	return {
		add: () => {
			dispatch({ type: 'ADD', factor: 5 })
		},
		sub: () => {
			dispatch({ type: 'SUB', factor: 5 })
		},
		alert: () => {
			dispatch({ type: 'alert', value: 0 })
		},
	}
}

export default connect(null, mapDispatch)(Controller);