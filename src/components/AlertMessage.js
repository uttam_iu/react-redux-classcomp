import React, { Component } from 'react';
import { connect } from 'react-redux';

class AlertMessage extends Component {
	render() {
		console.log(this.props)
		return (
			<div>
				{'alert message: ' + this.props.value}
			</div>
		);
	}
}

function mapStToPrps(state) {
	return {
		value: state.alertReducer.value
	}
}

export default connect(mapStToPrps)(AlertMessage);