import React, { Component } from 'react';
import { connect } from 'react-redux';

class Counters extends Component {
	render() {
		console.log(this.props)
		return (
			<div>
				{this.props.count}
			</div>
		);
	}
}

function mapStToPrps(state) {
	return {
		count: state.incDecReducer.count
	}
}

export default connect(mapStToPrps)(Counters);